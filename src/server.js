const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyparser = require("body-parser");

// root routers
const adminRoutes = require("./routes/adminRoutes");
const marketManagerRoutes = require("./routes/marketManagerRoutes");
const customerRoutes = require("./routes/customerRoutes");
const shopRoutes = require("./routes/shopRoutes");

app.use("/images", express.static("./images"));
app.use("/tumbnails", express.static("./tumbnails"));

// cors headers
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, PATCH, DELETE"
  );
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

// for application/json requests
app.use(bodyparser.json());

// root routes
app.use("/shop", shopRoutes);
app.use("/admin", adminRoutes);
app.use("/market", marketManagerRoutes);
app.use("/customer", customerRoutes);

// mongo connection and server starting point

//////////////////////
// TODO: add this to conf directore
// TODO: add required env variables
//////////////////////

mongoose
  .connect("mongodb://localhost:27017/online-shop", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then((result) => {
    const port = process.env.PORT || 5000;
    app.listen(port, () => {
      console.log(`server is running on port ${port}`);
    });
  })
  .catch((err) => {
    console.log("there is an error connecting to database.");
  });
