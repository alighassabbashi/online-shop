const multer = require("multer");
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    const type = `.${file.mimetype.split("/")[1]}`;
    cb(null, "image" + "-" + Date.now() + type);
  },
});
const upload = multer({ storage: storage });

module.exports = { upload };
