const fs = require("fs/promises");

const sharp = require("sharp");

const mainDir = require("../utils/path");

const { Product } = require("../models/product");
const { ProductDetails } = require("../models/productDetails");

exports.getTrendedProducts = async (req, res) => {
  console.log("get back landing page data");
};

exports.getCategoryProducts = async (req, res, next) => {
  try {
    const products = await Product.find({ category: req.params.categoryId });
    req.products = products;
    if (products.length === 0) {
      return res.status(200).send("no product in this category");
    }

    ///////////
    // TODO: handle pagination
    //////////

    res.status(200).send(products);
    next();
  } catch (err) {
    console.log("error getting category's product", err);
    res.status(500).send("error getting category's product");
  }
};

exports.getProduct = async (req, res, next) => {
  try {
    const product = await ProductDetails.findById(req.params.id).populate(
      "product_id"
    );
    req.product = await Product.findById(product.product_id);
    if (!product) {
      return res.status(404).send("product not found");
    }
    res.status(200).send(product);
    next();
  } catch (err) {
    console.log("error finding product", err);
    res.status(500).send("error finding product");
  }
};

exports.addRequiredInfo = async (req, res) => {
  const newProduct = new Product({
    schema_version: 3,
    name: req.body.name,
    price: req.body.price,
    discount: req.body.discount,
    special_offer: req.body.special_offer,
    brand: req.body.brand,
    segment: req.body.segment,
    category: req.body.category,
    view_count: 0,
  });

  try {
    const tempProduct = await newProduct.save();
    const productDetails = new ProductDetails({
      product_id: tempProduct._id,
    });
    const createdProductDetails = await productDetails.save();
    newProduct.details = createdProductDetails._id;
    const createdProduct = await newProduct.save();
    res.status(200).send({
      product: createdProduct._id,
      details: createdProductDetails._id,
    });
  } catch (err) {
    console.log("error saving new product", err);
    res.status(500).send("error saving new product");
  }
};

exports.addColoring = async (req, res) => {
  try {
    const productDetails = req.productDetails;
    let product = await Product.findById(productDetails.product_id);
    productDetails.colors = req.body.colors;
    const savedProductDetails = await productDetails.save();
    res.status(200).send({ message: "Created Successfully" });
    product.details = savedProductDetails._id;
    await product.save();
  } catch (err) {
    console.log("error saving product colors", err);
    res.status(500).send("error saving product colors");
  }
};

exports.addImages = async (req, res, next) => {
  try {
    req.images = [];
    let productDetails = req.productDetails;
    const newImages = req.files.map((file) => {
      return {
        alt: file.originalname,
        image: {
          path: mainDir + "/" + file.path,
          url: "http://localhost:5000/" + file.path,
        },
        name: file.filename,
      };
    });
    newImages.forEach((image) => {
      productDetails.images.push(image);
    });

    const productWithImages = await productDetails.save();
    req.images = productWithImages.images;
    res.status(200).send(productWithImages.images);
    next();
  } catch (err) {
    console.log("error saving product images", err);
    res.status(500).send("error saving product images");
  }
};

exports.deleteImage = async (req, res, next) => {
  try {
    let productDetails = req.productDetails;
    const product = await Product.findById(productDetails.product_id);
    req.mostDeleteImage = [];
    req.images = [];
    const mostDeleteImageIndex = productDetails.images.findIndex(
      (image) => image._id.toString() === req.body.imageId
    );
    const mostDeleteImage = productDetails.images.find(
      (image) => image._id.toString() === req.body.imageId
    );

    if (mostDeleteImageIndex === -1) {
      return res.status(404).send("there is no image with this id");
    }

    productDetails.images.splice(mostDeleteImageIndex, 1);
    product.tumbnails.splice(mostDeleteImageIndex, 1);
    await productDetails.save();
    await product.save();
    res.status(200).send({ message: "deleted successfully" });

    // most be delete from storage
    const deletedImageName = mostDeleteImage.name;
    req.images.push(deletedImageName);
    next();
  } catch (err) {
    console.log("error deleting image from product", err);
    res.status(500).send("error deleting image from product");
  }
};

exports.updateImagePosition = async (req, res, next) => {
  try {
    const productDetails = req.productDetails;
    const product = await Product.findById(productDetails.product_id);
    const imagePositoin = productDetails.images.findIndex((image) => {
      return image._id.toString() === req.body.imageId;
    });
    const tumbnailPosition = product.tumbnails.findIndex((image) => {
      return image.neme === req.productDetails.images[imagePositoin].name;
    });
    if (imagePositoin === -1) {
      return res.status(404).send("there is no image with this id");
    }
    const deletedImage = productDetails.images.splice(imagePositoin, 1);
    const deletedTumbnail = product.tumbnails.splice(tumbnailPosition, 1);

    productDetails.images.unshift(deletedImage[0]);
    product.tumbnails.unshift(deletedTumbnail[0]);

    await productDetails.save();
    await product.save();
    res.status(200).send({ message: "image position updated successfuly" });
    next();
  } catch (err) {
    console.log("error updateing product image position", err);
    res.status(500).send("error updateing product image position");
  }
};

exports.addFeatures = async (req, res) => {
  try {
    const productDetails = req.productDetails;
    productDetails.features = req.body.features;
    await productDetails.save();
    res.status(200).send({ message: "Created Successfully" });
  } catch (err) {
    console.log("error saving product features", err);
    res.status(500).send("error saving product features");
  }
};

exports.addProductReview = async (req, res) => {
  try {
    const productDetails = req.productDetails;
    productDetails.product_review = req.body.product_review;
    const productWithReview = await productDetails.save();
    res.status(200).send(productWithReview);
  } catch (err) {
    console.log("error saving product review", err);
    res.status(500).send("error saving product review");
  }
};

exports.updateProduct = async (req, res) => {
  try {
    let productDetails = req.productDetails;
    let product = await Product.findById(productDetails.product_id);
    if (!productDetails || !product) {
      return res.status(404).send("there is no product with this id");
    }
    productDetails.schema_version = 3;
    product.schema_version = 3;
    product.name = req.body.name || product.name;
    product.price = req.body.price || product.price;
    product.discount = req.body.discount || product.discount;
    product.special_offer = req.special_offer || product.special_offer;
    product.tumbnails = req.body.tumbnails || product.tumbnails;
    productDetails.model = req.body.model || productDetails.model;
    productDetails.images = req.body.images || productDetails.images;
    productDetails.colors = req.body.colors || productDetails.colors;
    productDetails.features = req.body.features || productDetails.features;
    productDetails.product_review =
      req.body.product_review || productDetails.product_review;
    product.brand = req.body.brand || product.brand;
    product.category = req.body.category || product.category;
    product.segment = req.body.segment || product.segment;

    let updatedProductDetails = await productDetails.save();
    const updatedProduct = await product.save();
    updatedProduct.details = updatedProductDetails;
    res.status(200).send(updatedProduct);
  } catch (err) {
    console.log("error updating product", err);
    res.status(500).send("error updating product");
  }
};

exports.toggleSpecialOffer = async (req, res) => {
  try {
    const product = await Product.findById(req.params.productId);
    if (!product) {
      res.status(404).send("there is no product with this id");
    }
    product.special_offer = !product.special_offer;
    await product.save();
    res.status(200).send(product.special_offer);
  } catch (err) {
    console.log("error toggling product special offer status", err);
    res.status(500).send("error toggling product special offer status");
  }
};

exports.deleteProduct = async (req, res, next) => {
  try {
    req.images = [];
    req.tumbnails = [];
    const deletedProduct = await Product.findByIdAndRemove(req.params.id);
    const deletedProductDetails = await ProductDetails.findByIdAndDelete(
      deletedProduct.details
    );
    if (!deletedProduct || !deletedProductDetails) {
      return res.status(404).send("there is no product with this id");
    }
    res.status(200).send({ message: "successfull deleted" });
    for (image in deletedProductDetails.images) {
      req.images.push(deletedProductDetails.images[image].name);
    }
    next();
  } catch (err) {
    console.log("error deleting product", err);
    res.status(500).send("error deleting product");
  }
};
