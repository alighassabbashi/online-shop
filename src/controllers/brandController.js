const fs = require("fs/promises");

const mainDir = require("../utils/path");

const { Brand } = require("../models/brand");

exports.getAllBrands = async (req, res) => {
  try {
    const brands = await Brand.find();
    if (!brands) {
      return res.status(404).send("there is no brand");
    }
    res.status(200).send(brands);
  } catch (err) {
    console.log("error getting all brands", err);
    res.status(500).send("error getting all brands");
  }
};

exports.getCategoryBrands = async (req, res) => {
  try {
    const brands = await Brand.find({ categories: req.params.categoryId });
    if (brands.length === 0) {
      return res.status(404).send("there is no brand with this categoryId");
    }
    res.status(200).send(brands);
  } catch (err) {
    console.log("error getting category brands", err);
    res.status(500).send("error getting category brands");
  }
};

exports.addNewBrand = async (req, res) => {
  const newBrand = new Brand({
    schema_version: 1,
    name: req.body.name,
    image: {
      path: mainDir + "/" + req.file.path,
      url: "http://localhost:5000" + "/" + req.file.path,
    },
    categories: req.body.categories,
    segments: req.body.segments,
  });

  try {
    const createdBrand = await newBrand.save();
    res.status(200).send(createdBrand);
  } catch (err) {
    console.log("error saving brand", err);
    res.status(500).send("error saving brand");
  }
};

exports.updateBrand = async (req, res) => {
  try {
    const brand = await Brand.findById(req.params.brandId);
    if (!brand) {
      return res.status(404).send("there is no brand with this id");
    }
    if (req.file) {
      try {
        await fs.rm(brand.image.path);
      } catch (err) {
        console.log("error deleting previus logo from storage", err);
      }
    }
    brand.schema_version = 1;
    brand.name = req.body.name || brand.name;
    brand.image = req.file
      ? {
          path: mainDir + "/" + req.file.path,
          url: "http://localhost:5000" + "/" + req.file.path,
        }
      : brand.image;
    brand.categories = req.body.categories || brand.categories;
    brand.segments = req.body.segments || brand.segments;
    const updatedBrand = await brand.save();
    res.status(200).send(updatedBrand);
  } catch (err) {
    res.status(500).send("error updating brand");
    console.log("error updating brand", err);
  }
};

exports.deleteBrand = async (req, res) => {
  try {
    const deletedBrand = await Brand.findByIdAndRemove(req.params.brandId);
    if (!deletedBrand) {
      return res.status(404).send("there is no brand with this id");
    }
    try {
      await fs.rm(deletedBrand.image.path);
    } catch (err) {
      console.log("error deleting brand image from storage", err);
    }
    res.status(200).send({ message: "successfull deleted" });
  } catch (err) {
    res.status(500).send("error deleting brand");
    console.log("error deleting brand", err);
  }
};
