const fs = require("fs/promises");
const path = require("path");

const mainDir = require("../utils/path");
const mostDeleteImgPath = require("../utils/mostDeleteImgPath");

const { Segment } = require("../models/segment");

exports.getSegmentsList = async (req, res) => {
  try {
    const segments = await Segment.find();
    if (!segments) {
      res.status(404).send("there is no segment in list");
    }
    res.status(200).send(segments);
  } catch (err) {
    res.status(500).send("error getting segments");
    console.log("error getting segments", err);
  }
};

exports.addNewSegment = async (req, res) => {
  const newSegment = new Segment({
    schema_version: 1,
    name: req.body.name,
    segmentIcon: req.body.segmentIcon,
    categories: [],
  });

  try {
    const createdSegment = await newSegment.save();
    res.status(200).send(createdSegment);
  } catch (err) {
    res.status(500).send("error saving new segment");
    console.log("error saving new segment", err);
  }
};

exports.updateSegment = async (req, res) => {
  try {
    const segment = req.segment;
    segment.name = req.body.name || segment.name;
    segment.segmentIcon = req.body.segmentIcon || segment.segmentIcon;
    segment.schema_version = 1;
    const updatedSegment = await segment.save();
    res.status(200).send(updatedSegment);
  } catch (err) {
    res.status(500).send("error updating segment data");
    console.log("error updating segment data", err);
  }
};

exports.deleteSegment = async (req, res) => {
  try {
    const deletedSegment = await Segment.findByIdAndRemove(req.params.id);
    if (!deletedSegment) {
      return res.status(404).send("there is no segment with this id");
    }
    res.status(200).send({ message: "successfull deleted" });
    try {
      for (let category in deletedSegment.categories) {
        await fs.rm(deletedSegment.categories[category].image.path);
      }
    } catch (err) {
      console.log("error deleting category image from storage", err);
    }
  } catch (err) {
    res.status(500).send("error deleteing segment");
    console.log("error deleting segment", err);
  }
};

exports.uploadCategoryImage = (req, res) => {
  const image = {
    path: mainDir + "/" + req.file.path,
    url: "http://localhost:5000/" + req.file.path,
  };
  if (!req.file) {
    return res.status(400).send("there is no image to upload");
  }
  res.status(200).send(image);
};

exports.addNewCategory = async (req, res) => {
  try {
    let segment = req.segment;
    const category = {
      schema_version: 1,
      name: req.body.name,
      image: req.body.image,
      features: req.body.features || [],
      recent_products: [],
    };
    segment.categories.push(category);
    const updatedSegment = await segment.save();
    res.status(200).send(updatedSegment.categories.reverse()[0]);
  } catch (err) {
    res.status(500).send("error adding new categories");
    console.log("error adding new categories", err);
  }
};

exports.updateCategory = async (req, res) => {
  try {
    let segment = req.segment;
    const categoryIndex = req.categoryIndex;
    segment.categories[categoryIndex].schema_version = 1;
    segment.categories[categoryIndex].name =
      req.body.name || segment.categories[categoryIndex].name;
    segment.categories[categoryIndex].image =
      req.body.image || segment.categories[categoryIndex].image;
    segment.categories[categoryIndex].features =
      req.body.features || segment.categories[categoryIndex].features;

    const updatedSegment = await segment.save();
    res.status(200).send(updatedSegment.categories[categoryIndex]);
  } catch (err) {
    res.status(500).send("error updating category");
    console.log("error updating category", err);
  }
};

exports.updateCategoryImage = async (req, res) => {
  try {
    let segment = req.segment;
    const categoryIndex = req.categoryIndex;
    const category = segment.categories.find((category) => {
      return category._id.toString() === req.body.categoryId;
    });
    try {
      await fs.unlink(category.image.path);
    } catch (err) {
      console.log("error deleting previus image", err);
    }
    category.image = {
      path: mainDir + "/" + req.file.path,
      url: "http://localhost:5000/" + req.file.path,
    };
    segment.categories.splice(categoryIndex, 1, category);
    await segment.save();
    res.status(200).send(category.image);
  } catch (err) {
    console.log("error updating category image", err);
    res.status(500).send("error updating category image");
  }
};

exports.deleteCategory = async (req, res) => {
  try {
    let segment = req.segment;
    const categoryIndex = req.categoryIndex;
    const deletedCategory = segment.categories.splice(categoryIndex, 1);
    try {
      await fs.rm(deletedCategory[0].image.path);
    } catch (err) {
      console.log("error deleting category image from storage", err);
    }
    await segment.save();
    res.status(200).send({ message: "successfull deleted" });
  } catch (err) {
    res.status(500).send("error adding new categories");
    console.log("error deleting category", err);
  }
};
