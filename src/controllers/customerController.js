const { User } = require("../models/user");

exports.getCustomer = (req, res) => {
  console.log("get information of a customer");
};

exports.signupNewCustomer = async (req, res) => {
  const user = new User({
    schema_version: 1,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    username: req.body.username,
    email: req.body.email,
    password: req.body.password,
  });

  try {
    const createdUser = await user.save();
    res.status(200).send(createdUser);
  } catch (err) {
    console.log("error creating new user", err);
    res.status(500).send("error creating new user");
  }
};

exports.loginUser = async (req, res) => {
  try {
    const user = await User.find({
      username: req.body.username,
      password: req.body.password,
    });
    console.log(user);
  } catch (err) {
    console.log(err);
  }
};

exports.updateCustomer = (req, res) => {
  console.log("update information of a customer");
};

exports.deleteCustomer = (req, res) => {
  console.log("delete a customer acount. maybe dont have it.");
};

/////////////////////////

exports.addToCart = (req, res) => {
  console.log("customer add a product to cart");
};

exports.removeFromCart = (req, res) => {
  console.log("customer remove a product from cart");
};

exports.changeQuantity = (req, res) => {
  console.log("customer change a product's quantity in cart");
};

exports.submitOrder = (req, res) => {
  console.log("customer make an order");
};
