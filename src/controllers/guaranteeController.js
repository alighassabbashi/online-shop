const { Guarantee } = require("../models/guarantee");

exports.getAllGuarantee = async (req, res) => {
  try {
    const Guarantees = await Guarantee.find();
    if (!Guarantees) {
      return res.status(404).send("there is no guarantee");
    }
    res.status(200).send(Guarantees);
  } catch (err) {
    console.log("error getting all guarantees", err);
    res.status(500).send("error getting all guarantees");
  }
};

exports.addNewGuarantee = async (req, res) => {
  const newGuarantee = new Guarantee({
    schema_version: 1,
    name: req.body.name,
    durations: req.body.durations,
    brands: req.body.brands,
  });

  try {
    const createdGuarantee = await newGuarantee.save();
    res.status(200).send(createdGuarantee);
  } catch (err) {
    console.log("error saving new guarantee", err);
    res.status(500).send("error saving new guarantee");
  }
};

exports.updateGuarantee = async (req, res) => {
  try {
    const guarantee = await Guarantee.findById(req.params.guaranteeId);
    if (!guarantee) {
      return res.status(404).send("there is no guarantee with this id");
    }

    guarantee.schema_version = 1;
    guarantee.name = req.body.name || guarantee.name;
    guarantee.durations = req.body.durations || guarantee.durations;
    guarantee.barnds = req.body.barnds || guarantee.barnds;
    const updatedGuarantee = await guarantee.save();
    res.status(200).send(updatedGuarantee);
  } catch (err) {
    res.status(500).send("error updating guarantee");
    console.log("error updating guarantee", err);
  }
};

exports.deleteGuarantee = async (req, res) => {
  try {
    const deletedGuarantee = await Guarantee.findByIdAndRemove(
      req.params.guaranteeId
    );
    if (!deletedGuarantee) {
      return res.status(404).send("there is no guarantee with this id");
    }
    res.status(200).send({ message: "successfull deleted" });
  } catch (err) {
    res.status(500).send("error deleting guarantee");
    console.log("error deleting guarantee", err);
  }
};
