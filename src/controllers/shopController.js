const { Product } = require("../models/product");

exports.getMostVisitedProduts = async (req, res) => {
  try {
    const mostVisitedProducts = await Product.find()
      .sort("-view_count")
      .limit(10);
    if (mostVisitedProducts.length === 0) {
      return res.status(404).send("there is no most visited product list");
    }
    res.send(mostVisitedProducts);
  } catch (err) {
    console.log("error getting most visited products", err);
    res.status(500).send("error getting most visited products");
  }
};

exports.getSpecialOfferProducts = async (req, res) => {
  try {
    const specialOfferProducts = await Product.find({ special_offer: true })
      .sort("createdAt")
      .limit(10);
    if (specialOfferProducts.length === 0) {
      return res.status(404).send("there is no special product ");
    }
    res.send(specialOfferProducts);
  } catch (err) {
    console.log("error getting special offer products", err);
    res.status(500).send("error getting special offer products");
  }
};

exports.getVeryLastProducts = async (req, res) => {
  try {
    const veryLastProducts = await Product.find().sort("createdAt").limit(10);
    if (veryLastProducts.length === 0) {
      return res.status(404).send("there is no product ");
    }
    res.send(veryLastProducts);
  } catch (err) {
    console.log("error getting very last products", err);
    res.status(500).send("error getting very last products");
  }
};

exports.getdiscountedProducts = async (req, res) => {
  try {
    const discountedProducts = await Product.find({
      discount: { $ne: null, $ne: 0 },
    })
      .sort("createdAt")
      .limit(10);
    if (discountedProducts.length === 0) {
      return res.status(404).send("there is no discounted product ");
    }
    res.send(discountedProducts);
  } catch (err) {
    console.log("error getting discounted products", err);
    res.status(500).send("error getting discounted products");
  }
};
