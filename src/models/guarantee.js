const mongoose = require("mongoose");
const { Schema } = mongoose;

const GuaranteeSchema = new Schema({
  schema_version: Number,
  name: {
    type: String,
    required: true,
  },
  durations: {
    type: [Number],
    required: true,
  },
  brands: {
    type: [mongoose.Types.ObjectId],
    index: true,
    ref: "Brand",
  },
});

const Guarantee = mongoose.model("Guarantee", GuaranteeSchema);

module.exports = { Guarantee };
