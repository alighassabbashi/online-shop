const mongoose = require("mongoose");
const { Schema } = mongoose;

const ProductDetailsSchema = new Schema(
  {
    schema_version: Number,
    product_id: {
      type: mongoose.Types.ObjectId,
      ref: "Product",
      required: true,
    },
    model: {
      type: Number,
      defualt: 1,
    },
    images: [
      {
        alt: String,
        image: {
          url: String,
          path: String,
        },
        name: String,
      },
    ],
    colors: [
      {
        color: String,
        color_name: String,
        addedCost: Number,
        quantity: {
          type: Number,
          required: true,
        },
        guarantee: {
          type: mongoose.Types.ObjectId,
          ref: "Guarantee",
        },
        guarantee_duration: {
          type: Number,
        },
      },
    ],
    features: [
      {
        name: String,
        pairs: [
          {
            k: String,
            v: String,
            is_special: {
              type: Boolean,
              default: false,
            },
          },
        ],
      },
    ],
    product_review: {
      type: String,
    },
  },
  { timestamps: true }
);

ProductDetailsSchema.index({
  "features.pairs.k": 1,
  "features.pairs.v": 1,
});

const ProductDetails = mongoose.model("ProductDetails", ProductDetailsSchema);

module.exports = { ProductDetails };
