const mongoose = require("mongoose");
const { Schema } = mongoose;

const UserSchema = new Schema({
  schema_version: Number,
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  username: {
    type: String,
    required: true,
    trim: true,
    minlength: 6,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
    email: true,
  },
  addresses: [
    {
      type: String,
    },
  ],
  cart: [
    {
      product: {
        type: mongoose.Types.ObjectId,
        ref: "Product",
      },
      quantity: {
        type: Number,
        required: true,
      },
      total_cost: {
        type: Number,
        required: true,
      },
    },
  ],
});

UserSchema.index({ username: 1, email: 1 });

const User = mongoose.model("User", UserSchema);

module.exports = { User };
