const mongoose = require("mongoose");
const { Schema } = mongoose;

const PaymentSchema = new Schema(
  {
    schema_version: Number,
    date: Date,
    customer: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
      index: true,
    },
    order: [
      {
        product: {
          type: mongoose.Types.ObjectId,
          ref: "Product",
          required: true,
          index: true,
        },
        quantity: Number,
      },
    ],
  },
  { timestamps: true }
);

const Payment = mongoose.model("Payment", PaymentSchema);

module.exports = { Payment };
