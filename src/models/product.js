const mongoose = require("mongoose");
const { Schema } = mongoose;

const ProductSchema = new Schema(
  {
    schema_version: Number,
    name: {
      type: String,
      required: true,
      index: true,
    },
    price: {
      type: Number,
      required: true,
      index: true,
    },
    discount: {
      type: Number,
      index: true,
    },
    tumbnails: [
      {
        alt: String,
        image: {
          url: String,
          path: String,
        },
      },
    ],
    brand: {
      type: mongoose.Types.ObjectId,
      ref: "Brand",
      required: true,
      index: true,
    },
    category: {
      type: mongoose.Types.ObjectId,
      required: true,
      index: true,
    },
    segment: {
      type: mongoose.Types.ObjectId,
      required: true,
      index: true,
    },
    view_count: {
      type: Number,
      index: true,
    },
    special_offer: {
      type: Boolean,
      default: false,
    },
    details: {
      type: mongoose.Types.ObjectId,
      ref: "ProductDetails",
    },
  },
  { timestamps: true }
);

const Product = mongoose.model("Product", ProductSchema);

module.exports = { Product };
