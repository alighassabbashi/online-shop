const mongoose = require("mongoose");
const { Schema } = mongoose;

const SegmentSchema = new Schema({
  schema_version: Number,
  name: {
    type: String,
    required: true,
  },
  segmentIcon: {
    type: String,
  },
  categories: [
    {
      schema_version: Number,
      name: {
        type: String,
        required: true,
      },
      view_count: {
        type: Number,
        index: true,
      },
      image: {
        url: String,
        path: String,
      },
      features: [
        {
          name: String,
          pairs: [
            {
              k: String,
            },
          ],
        },
      ],
      recent_products: [
        {
          product: {
            type: mongoose.Types.ObjectId,
            ref: "Product",
            required: true,
          },
        },
      ],
    },
  ],
});

const Segment = mongoose.model("Segment", SegmentSchema);

module.exports = { Segment };
