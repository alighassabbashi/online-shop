const mongoose = require("mongoose");
const { Schema } = mongoose;

const BrandSchema = new Schema({
  schema_version: Number,
  name: {
    type: String,
    required: true,
  },
  image: {
    url: String,
    path: String,
  },
  segments: {
    type: [mongoose.Types.ObjectId],
    index: true,
  },
  categories: {
    type: [mongoose.Types.ObjectId],
    index: true,
  },
});

const Brand = mongoose.model("Brand", BrandSchema);

module.exports = { Brand };
