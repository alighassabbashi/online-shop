const { Segment } = require("../models/segment");

const findSegmentAndCategory = async (req, res, next) => {
  try {
    let segment = await Segment.findById(req.params.segmentId);
    if (!segment) {
      const error = new Error("there is no segment with this id");
      next(res.status(404).send(error.message));
    }
    req.segment = segment;
    if (req.body.categoryId) {
      let categoryIndex = segment.categories.findIndex(
        (category) => category._id.toString() === req.body.categoryId
      );
      if (categoryIndex === -1) {
        const error = new Error("there is no category with this id");
        next(res.status(404).send(error.message));
      }
      req.categoryIndex = categoryIndex;
    }
    next();
  } catch (error) {
    next(error.message);
  }
};

module.exports = { findSegmentAndCategory };
