const fs = require("fs/promises");

const sharp = require("sharp");

const { Product } = require("../models/product");
const { pathGen } = require("../utils/tumbnailPath");

const handleTumbnail = async (req, res, next) => {
  try {
    const images = req.images;
    const product = await Product.findById(req.productDetails.product_id);
    const newTumbnails = [];
    for (const image in images) {
      const pathes = pathGen(images[image].name);
      sharp(pathes.inputImage.path)
        .resize(120)
        .toFile(pathes.outputTumbnail.path);
      newTumbnails.push({
        alt: images[image].alt,
        image: { path: pathes.tumbnailUrl.path, url: pathes.tumbnailUrl.url },
      });
    }
    if (newTumbnails.length !== 0) {
      product.tumbnails = newTumbnails;
      await product.save();
    }
    next();
  } catch (err) {
    console.log("error creating tumbnails", err);
  }
};

module.exports = { handleTumbnail };
