const fs = require("fs/promises");

const mostDeleteImagePath = require("../utils/mostDeleteImgPath");

module.exports.deleteImageFromStorage = async (req, res, next) => {
  const images = req.images;
  for (image in images) {
    const pathToFile = mostDeleteImagePath(images[image]);
    const imageUrl = pathToFile.image;
    const tumbnailUrl = pathToFile.tumbnail;
    try {
      await fs.rm(imageUrl);
      await fs.rm(tumbnailUrl);
      console.log(
        `image and tumbnail for --${images[image]} deleted successfully`
      );
    } catch (err) {
      console.log(`image --${images[image]} not exist`);
    }
  }
  next();
};
