const { ProductDetails } = require("../models/productDetails");

const findProductDetails = async (req, res, next) => {
  try {
    let productDetails = await ProductDetails.findById(req.params.productId);
    if (!productDetails) {
      const error = new Error("there is no product with this id");
      next(res.status(404).send(error.message));
    }
    req.productDetails = productDetails;
    next();
  } catch (error) {
    next(error.message);
  }
};

module.exports = { findProductDetails };
