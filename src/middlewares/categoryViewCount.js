const { Segment } = require("../models/segment");

const addCategoryViewCount = async (req) => {
  try {
    const segment = await Segment.findById(req.products[0].segment);
    let categoryIndex = segment.categories.findIndex(
      (category) => category._id.toString() === req.params.categoryId
    );
    if (segment.categories[categoryIndex].schema_version === 1) {
      segment.categories[categoryIndex].schema_version = 2;
      segment.categories[categoryIndex].view_count = 1;
      await segment.save();
    } else if (segment.categories[categoryIndex].schema_version === 2) {
      segment.categories[categoryIndex].view_count++;
      await segment.save();
    }
    console.log(segment);
  } catch (err) {
    console.log(err);
  }
};

module.exports = { addCategoryViewCount };
