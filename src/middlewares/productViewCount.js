const addProductViewCount = async (req) => {
  try {
    let product = req.product;
    if (product.schema_version === 1 || product.schema_version === 2) {
      product.schema_version = 3;
      product.view_count = 1;
      await product.save();
    } else {
      product.view_count++;
      await product.save();
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = { addProductViewCount };
