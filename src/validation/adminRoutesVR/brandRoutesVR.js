const { body } = require("express-validator");

const addNewBrandR = () => {
  return [
    body("name").notEmpty().withMessage("وارد کردن نام برند الزامی است"),
    body(["categories", "segments"])
      .notEmpty()
      .withMessage("آی دی دسته بندی و قسمت الزامی است")
      .customSanitizer((arrays) => {
        return JSON.parse(arrays);
      }),
  ];
};

module.exports = { addNewBrandR };
