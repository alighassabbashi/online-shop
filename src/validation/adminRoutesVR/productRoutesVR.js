const { body, validationResult } = require("express-validator");

const requiredInfoR = () => {
  return [
    body("name").notEmpty().withMessage("وارد کردن نام محصول الزامی است"),
    body("price")
      .notEmpty()
      .isFloat({ min: 2000 })
      .withMessage("محصول نباید ارزان تر از ۲۰۰۰ تومان باشد"),
    body("discount")
      .notEmpty()
      .isInt({ min: 0, max: 100 })
      .withMessage(
        "تخفیف باید برحسب درصد بوده و عددی بین ۰ تا ۱۰۰ باشد و الزامی است"
      ),
    body(["brand", "category", "segment"])
      .notEmpty()
      .withMessage("آی دی برند، دسته بندی و قسمت الزامی است"),
  ];
};

const updateProductR = () => {
  return [
    body("price")
      .if(body("price").exists())
      .isFloat({ min: 2000 })
      .withMessage("محصول نباید ارزان تر از ۲۰۰۰ تومان باشد"),
    body("discount")
      .if(body("discount").exists())
      .isInt({ min: 0, max: 100 })
      .withMessage(
        "تخفیف باید برحسب درصد بوده و عددی بین ۰ تا ۱۰۰ باشد و الزامی است"
      ),
  ];
};

const validation = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    console.log("ali reched here");
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));

  return res.status(422).send({ errors: extractedErrors });
};

module.exports = { requiredInfoR, updateProductR, validation };
