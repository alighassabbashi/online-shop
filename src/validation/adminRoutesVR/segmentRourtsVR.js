const { body } = require("express-validator");

const addNewR = () => {
  return [body("name").notEmpty().withMessage("وارد کردن نام قسمت الزامی است")];
};

module.exports = { addNewR };
