const { body } = require("express-validator");

const addNewGuaranteeR = () => {
  return [
    body("name").notEmpty().withMessage("وارد کردن نام گارانتی الزامی است"),
  ];
};

module.exports = { addNewGuaranteeR };
