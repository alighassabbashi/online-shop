const path = require("path");
const pathToFile = process.env.MODE
  ? "https://online-market-backend-iwanttodoit.fandogh.cloud/"
  : path.join(path.dirname(require.main.filename), "..");

module.exports = pathToFile;
