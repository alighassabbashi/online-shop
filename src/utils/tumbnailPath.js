const mainDir = require("./path");

const pathGen = (imageName) => {
  const inputImage = process.env.MODE
    ? `/app/images/${imageName}`
    : {
        path: mainDir + "/images/" + imageName,
        url: "http://localhost:5000" + "/images/" + imageName,
      };
  const outputTumbnail = process.env.MODE
    ? `/app/tumbnails/${imageName}`
    : {
        path: mainDir + "/tumbnails/" + imageName,
        url: "http://localhost:5000" + "/tumbnails/" + imageName,
      };
  const tumbnailUrl = process.env.MODE
    ? "https://online-market-backend-iwanttodoit.fandogh.cloud/tumbnails/" +
      imageName
    : {
        path: mainDir + "/tumbnails/" + imageName,
        url: "http://localhost:5000" + "/tumbnails/" + imageName,
      };

  return { inputImage, outputTumbnail, tumbnailUrl };
};

module.exports = { pathGen };
