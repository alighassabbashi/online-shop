const path = require("path");

module.exports = (imageName) => {
  const pathToImage = path.join(
    path.dirname(require.main.filename),
    "..",
    "images",
    imageName
  );
  const pathToTumbnail = path.join(
    path.dirname(require.main.filename),
    "..",
    "tumbnails",
    imageName
  );
  return { image: pathToImage, tumbnail: pathToTumbnail };
};
