const express = require("express");
const router = express.Router();

// middlewares and libs
const { upload } = require("../../configs/multerCustom");
const {
  deleteImageFromStorage,
} = require("../../middlewares/deleteImageFromStorage");
const { handleTumbnail } = require("../../middlewares/handleProductTumbnail");
const {
  requiredInfoR,
  updateProductR,
} = require("../../validation/adminRoutesVR/productRoutesVR");
const { validation } = require("../../validation/validation");
const { findProductDetails } = require("../../middlewares/findProductDetails");

// controller
const productControler = require("../../controllers/productsConroller");

// localhost:5000/admin/product/requiredInfo
router.post(
  "/requiredInfo",
  requiredInfoR(),
  validation,
  productControler.addRequiredInfo
);

// localhost:5000/admin/product/coloring
router.post(
  "/coloring/:productId",
  validation,
  findProductDetails,
  productControler.addColoring
);

// localhost:5000/admin/product/images/:productId
router.post(
  "/images/:productId",
  upload.array("images"),
  findProductDetails,
  productControler.addImages,
  handleTumbnail
);

// localhost:5000/admin/product/images/:productId
router.delete(
  "/images/:productId",
  findProductDetails,
  productControler.deleteImage,
  deleteImageFromStorage
);

// localhost:5000/admin/product/images/:productId
router.patch(
  "/images/:productId",
  findProductDetails,
  productControler.updateImagePosition
);

// localhost:5000/admin/product/features
router.post(
  "/features/:productId",
  findProductDetails,
  productControler.addFeatures
);

// localhost:5000/admin/product/review
router.post(
  "/review/:productId",
  findProductDetails,
  productControler.addProductReview
);
// localhost:5000/admin/product/special/:productId
router.post("/special/:productId", productControler.toggleSpecialOffer);

// localhost:5000/admin/product/:id
router.patch(
  "/:productId",
  updateProductR(),
  validation,
  findProductDetails,
  productControler.updateProduct
);

// localhost:5000/admin/product/:id
router.delete("/:id", productControler.deleteProduct, deleteImageFromStorage);

module.exports = router;
