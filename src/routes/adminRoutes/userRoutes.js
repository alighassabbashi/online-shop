const express = require("express");
const router = express.Router();

// controller
const adminController = require("../../controllers/adminController");

// localhost:5000/admin/user
router.get("/", adminController.getAdmin);

// localhost:5000/admin/user/changeUserType
router.patch("/changeUserType", adminController.updateUserType);

// localhost:5000/admin/user/banUser
router.patch("/banUser", adminController.banUser);

// localhost:5000/admin/user
router.patch("/", adminController.updateAdmin);

// localhost:5000/admin/user
router.delete("/", adminController.deleteAdmin);

module.exports = router;
