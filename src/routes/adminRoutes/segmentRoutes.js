const express = require("express");
const router = express.Router();

// middlewares and libs
const { upload } = require("../../configs/multerCustom");
const { addNewR } = require("../../validation/adminRoutesVR/segmentRourtsVR");
const { validation } = require("../../validation/validation");
const {
  findSegmentAndCategory,
} = require("../../middlewares/findSegmentAndCategory");

// controller
const segmentController = require("../../controllers/segmentsController");

// localhost:5000/admin/segment
router.post("/", addNewR(), validation, segmentController.addNewSegment);

// localhost:5000/admin/segment/:id
router.patch(
  "/:segmentId",
  findSegmentAndCategory,
  segmentController.updateSegment
);

// localhost:5000/admin/segment/:id
router.delete("/:id", segmentController.deleteSegment);

// localhost:5000/admin/segment/addCategory/:segmentId
router.post(
  "/addCategory/:segmentId",
  addNewR(),
  validation,
  findSegmentAndCategory,
  segmentController.addNewCategory
);

// localhost:5000/admin/segment/addCategoryImage
router.post(
  "/addCategoryImage",
  upload.single("image"),
  segmentController.uploadCategoryImage
);

// localhost:5000/admin/segment/updateCategory/:segmentId
router.patch(
  "/updateCategory/:segmentId",
  findSegmentAndCategory,
  segmentController.updateCategory
);

// localhost:5000/admin/segment/updateCategoryImage/:segmentId
router.patch(
  "/updateCategoryImage/:segmentId",
  upload.single("image"),
  findSegmentAndCategory,
  segmentController.updateCategoryImage
);

// localhost:5000/admin/segment/deleteCategory/:segmentId
router.delete(
  "/deleteCategory/:segmentId",
  findSegmentAndCategory,
  segmentController.deleteCategory
);

module.exports = router;
