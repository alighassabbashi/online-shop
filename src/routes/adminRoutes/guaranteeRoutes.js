const express = require("express");
const router = express.Router();

// middlewares and libs
const {
  addNewGuaranteeR,
} = require("../../validation/adminRoutesVR/guaranteeRoutesVR");
const { validation } = require("../../validation/validation");

// controller
const guaranteeController = require("../../controllers/guaranteeController");

// localhost:5000/admin/guarantee/all
router.get("/all", guaranteeController.getAllGuarantee);

// localhost:5000/admin/guarantee
router.post(
  "/",
  addNewGuaranteeR(),
  validation,
  guaranteeController.addNewGuarantee
);

// localhost:5000/admin/brand/:guaranteeId
router.patch(
  "/:guaranteeId",
  addNewGuaranteeR(),
  validation,
  guaranteeController.updateGuarantee
);

// localhost:5000/admin/brand/:guaranteeId
router.delete("/:guaranteeId", guaranteeController.deleteGuarantee);

module.exports = router;
