const express = require("express");
const router = express.Router();

// middlewares and libs
const { upload } = require("../../configs/multerCustom");
const {
  addNewBrandR,
} = require("../../validation/adminRoutesVR/brandRoutesVR");
const { validation } = require("../../validation/validation");

// controller
const brandController = require("../../controllers/brandController");

// localhost:5000/admin/brand/all
router.get("/all", brandController.getAllBrands);

// localhost:5000/admin/brand/categoryBrands/:categoryId
router.get("/categoryBrands/:categoryId", brandController.getCategoryBrands);

// localhost:5000/admin/brand
router.post(
  "/",
  upload.single("image"),
  addNewBrandR(),
  validation,
  brandController.addNewBrand
);

// localhost:5000/admin/brand/:brandId
router.patch("/:brandId", upload.single("image"), brandController.updateBrand);

// localhost:5000/admin/brand/:brandId
router.delete("/:brandId", brandController.deleteBrand);

module.exports = router;
