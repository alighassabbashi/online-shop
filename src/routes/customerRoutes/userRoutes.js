const express = require("express");
const router = express.Router();

const customerController = require("../../controllers/customerController");

// localhost:5000/customer
router.get("/", customerController.getCustomer);

// localhost:5000/customer/signup
router.post("/signup", customerController.signupNewCustomer);

// localhost:5000/customer/login
router.post("/login", customerController.loginUser);

// localhost:5000/customer
router.patch("/", customerController.updateCustomer);

// localhost:5000/customer
router.delete("/", customerController.deleteCustomer);

module.exports = router;
