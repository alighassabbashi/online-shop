const express = require("express");
const router = express.Router();

const customerController = require("../../controllers/customerController");

// customer add product to cart
router.patch("/add-to-cart", customerController.addToCart);

// customer remove product from cart
router.patch("/remove-from-cart", customerController.removeFromCart);

// customer change product's qiantity in cart
router.patch("/change-quantity", customerController.changeQuantity);

// customer submit an order
router.post("/submitOrder", customerController.submitOrder);

module.exports = router;
