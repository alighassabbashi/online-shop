const express = require('express')
const router = express.Router();

const marketController = require('../controllers/marketController');

// get information of a market manager
router.get('/', marketController.getMarket);
////
// rest of market manager routes
////


module.exports = router;