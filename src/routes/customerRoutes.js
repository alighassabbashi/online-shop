const express = require("express");
const router = express.Router();

// sub user routes
const userRoutes = require("./customerRoutes/userRoutes");

// sub card routes
const cardRoutes = require("./customerRoutes/cardRoutes");

///////////////////////////
// localhost:5000/customer
router.use("/", userRoutes);
///////////////////////////

///////////////////////////
// localhost:5000/customer/card
router.use("/card", cardRoutes);
///////////////////////////

module.exports = router;
