const express = require("express");
const router = express.Router();

const segmentController = require("../controllers/segmentsController");
const productControler = require("../controllers/productsConroller");
const shopControler = require("../controllers/shopController");

const { addCategoryViewCount } = require("../middlewares/categoryViewCount");
const { addProductViewCount } = require("../middlewares/productViewCount");

// retutns back all trended products for each segment
router.get("/", productControler.getTrendedProducts);

// returns the list of all segments
router.get("/segment", segmentController.getSegmentsList);

// return the list of all products in a category
router.get(
  "/category/:categoryId",
  productControler.getCategoryProducts,
  addCategoryViewCount
);

///////////////////////////

// return a single product
router.get("/product/:id", productControler.getProduct, addProductViewCount);

//////////////////////////

router.get("/mostVisited", shopControler.getMostVisitedProduts);

router.get("/specials", shopControler.getSpecialOfferProducts);

router.get("/veryLast", shopControler.getVeryLastProducts);

router.get("/discounted", shopControler.getdiscountedProducts);

module.exports = router;
