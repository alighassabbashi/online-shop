const express = require("express");
const router = express.Router();

// sub user routes
const userRoutes = require("./adminRoutes/userRoutes");

// sub product routes
const productRoutes = require("./adminRoutes/productRoutes");

// sub segment routes
const segmentRoutes = require("./adminRoutes/segmentRoutes");

// sub brand routes
const brandRoutes = require("./adminRoutes/brandRoutes");

// sub guarantee routes
const guaranteeRoutes = require("./adminRoutes/guaranteeRoutes");

///////////////////////////
// localhost:5000/admin/user
router.use("/user", userRoutes);
///////////////////////////

///////////////////////////
// localhost:5000/admin/product
router.use("/product", productRoutes);
///////////////////////////

///////////////////////////
// localhost:5000/admin/segment
router.use("/segment", segmentRoutes);
///////////////////////////

///////////////////////////
// localhost:5000/admin/brand
router.use("/brand", brandRoutes);
///////////////////////////

///////////////////////////
// localhost:5000/admin/guarantee
router.use("/guarantee", guaranteeRoutes);
///////////////////////////

module.exports = router;
